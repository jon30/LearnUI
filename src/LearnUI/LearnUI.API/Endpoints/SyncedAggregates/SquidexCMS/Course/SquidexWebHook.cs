using Carter;
using LearnUI.API.Domain;
using LearnUI.API.Domain.SyncedAggregates;
using LearnUI.API.Endpoints.SquidexSync.Customer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace LearningApi.Features.SyncedAggregates.CMS.Content;

public class SyncContent : ICarterModule
{
    public void AddRoutes(IEndpointRouteBuilder app)
    {
        app.MapPost("sync/squidex/content", Handler)
            .ExcludeFromDescription();
    }

    private async Task<IResult> Handler([FromServices] LearnUIDBContext dbContext, [FromServices] IConfiguration configuration, ContentChangedWrapper.ContentChangedEvent eventData, IOptions<SquidexOptions> squidexOptions, HttpContext context)
    {
        if (!await context.SquidexSignatureValid(squidexOptions.Value.SigningSecret)) return Results.Problem("Unable to validate signature");

        var existing = await dbContext.Content.FindAsync(Guid.Parse(eventData.Payload.Id));
        if (existing == null)
        {
            existing = new Course();
            dbContext.Content.Add(existing);
        }

        existing.Id = Guid.Parse(eventData.Payload.Id);
        existing.ContentId = eventData.Payload.Data.ContentId.Iv;
        existing.Name = eventData.Payload.Data.Name.Iv;
        existing.Format = eventData.Payload.Data.Format.Iv.FirstOrDefault();
        existing.Description = eventData.Payload.Data.Description.Iv;
        existing.LastUpdated = eventData.Timestamp;
        existing.DirectPurchaseProduct = eventData.Payload.Data.DirectPurchaseProduct.Iv.FirstOrDefault();

        await dbContext.SaveChangesAsync();

        return Results.Ok();
    }
}

