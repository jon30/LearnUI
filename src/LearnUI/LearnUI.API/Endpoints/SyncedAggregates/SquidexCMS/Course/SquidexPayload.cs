// Root myDeserializedClass = JsonSerializer.Deserialize<Root>(myJsonResponse);

using System.Text.Json.Serialization;

public class ContentChangedWrapper
{
// Root myDeserializedClass = JsonSerializer.Deserialize<Root>(myJsonResponse);
    public record ContentId
    {
        [JsonPropertyName("iv")] public string Iv { get; set; }
    }

    public record Name
    {
        [JsonPropertyName("iv")] public string Iv { get; set; }
    }

    public record Description
    {
        [JsonPropertyName("iv")] public string Iv { get; set; }
    }

    public record Level
    {
        [JsonPropertyName("iv")]  public int? Iv { get; set; }
    }

    public record Format
    {
        [JsonPropertyName("iv")] public List<string> Iv { get; set; }
    }

    public record LocationOnDisk
    {
        [JsonPropertyName("iv")] public string Iv { get; set; }
    }

    public record Cover
    {
        [JsonPropertyName("iv")] public List<string> Iv { get; set; }
    }

    public record DirectPurchaseProduct
    {
        [JsonPropertyName("iv")] public List<string> Iv { get; set; }
    }

    public record Downloads
    {
        [JsonPropertyName("iv")] public List<string> Iv { get; set; }
    }

    public record Data
    {
        [JsonPropertyName("ContentId")] public ContentId ContentId { get; set; }

        [JsonPropertyName("Name")] public Name Name { get; set; }

        [JsonPropertyName("Description")] public Description Description { get; set; }

        [JsonPropertyName("Format")] public Format Format { get; set; }

        [JsonPropertyName("LocationOnDisk")] public LocationOnDisk LocationOnDisk { get; set; }

        [JsonPropertyName("Cover")] public Cover Cover { get; set; }

        [JsonPropertyName("DirectPurchaseProduct")]
        public DirectPurchaseProduct DirectPurchaseProduct { get; set; }

        [JsonPropertyName("Downloads")] public Downloads Downloads { get; set; }
        
        [JsonPropertyName("Level")] public Level? Level { get; set; }
    }

    public record DataOld
    {
        [JsonPropertyName("ContentId")] public ContentId ContentId { get; set; }

        [JsonPropertyName("Name")] public Name Name { get; set; }

        [JsonPropertyName("Description")] public Description Description { get; set; }

        [JsonPropertyName("Format")] public Format Format { get; set; }

        [JsonPropertyName("LocationOnDisk")] public LocationOnDisk LocationOnDisk { get; set; }

        [JsonPropertyName("Cover")] public Cover Cover { get; set; }

        [JsonPropertyName("DirectPurchaseProduct")]
        public DirectPurchaseProduct DirectPurchaseProduct { get; set; }

        [JsonPropertyName("Downloads")] public Downloads Downloads { get; set; }
    }

    public record Payload
    {
        [JsonPropertyName("$type")] public string Type { get; set; }

        [JsonPropertyName("type")] public string ContentType { get; set; }

        [JsonPropertyName("id")] public string Id { get; set; }

        [JsonPropertyName("created")] public DateTime Created { get; set; }

        [JsonPropertyName("lastModified")] public DateTime LastModified { get; set; }

        [JsonPropertyName("createdBy")] public string CreatedBy { get; set; }

        [JsonPropertyName("lastModifiedBy")] public string LastModifiedBy { get; set; }

        [JsonPropertyName("data")] public Data Data { get; set; }

        [JsonPropertyName("dataOld")] public DataOld DataOld { get; set; }

        [JsonPropertyName("status")] public string Status { get; set; }

        [JsonPropertyName("partition")] public int Partition { get; set; }

        [JsonPropertyName("schemaId")] public string SchemaId { get; set; }

        [JsonPropertyName("actor")] public string Actor { get; set; }

        [JsonPropertyName("appId")] public string AppId { get; set; }

        [JsonPropertyName("timestamp")] public DateTime Timestamp { get; set; }

        [JsonPropertyName("name")] public string Name { get; set; }

        [JsonPropertyName("version")] public int Version { get; set; }
    }

    public record ContentChangedEvent
    {
        [JsonPropertyName("type")] public string Type { get; set; }

        [JsonPropertyName("payload")] public Payload Payload { get; set; }

        [JsonPropertyName("timestamp")] public DateTime Timestamp { get; set; }
    }
}