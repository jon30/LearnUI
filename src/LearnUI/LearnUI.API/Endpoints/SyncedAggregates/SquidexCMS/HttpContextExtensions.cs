﻿using LearnUI.API.Endpoints.SquidexSync.Customer;
using Microsoft.Extensions.Options;

namespace LearningApi.Features.SyncedAggregates.CMS.Content;

public static class HttpContextExtensions
{
    public static async Task<bool> SquidexSignatureValid(this HttpContext context, string signingSecret)
    {
        var rawBody = await context.Request.GetRawBodyAsync();
        var signature = $"{rawBody}{signingSecret}".Sha256Base64();
        return signature == context.Request.Headers["X-Signature"];
    }
}