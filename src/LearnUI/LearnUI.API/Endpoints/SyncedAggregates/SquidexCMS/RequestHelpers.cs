﻿using System.Text;

namespace LearnUI.API.Endpoints.SquidexSync.Customer;

public static class RequestHelpers
{
    public static async Task<string> GetRawBodyAsync(this HttpRequest request, Encoding encoding = null)
    {
        if (!request.Body.CanSeek)
        {
            request.EnableBuffering();
        }

        request.Body.Position = 0;

        using var reader = new StreamReader(request.Body, encoding);
        var body = await reader.ReadToEndAsync();
        request.Body.Position = 0;
        return body;
    }
}