// Root myDeserializedClass = JsonSerializer.Deserialize<Root>(myJsonResponse);

using System.Text.Json.Serialization;

public record Email
{
    [JsonPropertyName("iv")] public string Iv { get; set; }
}

public record NormalizedEmail
{
    [JsonPropertyName("iv")] public string Iv { get; set; }
}

public record Data
{
    [JsonPropertyName("Email")] public Email Email { get; set; }

    [JsonPropertyName("NormalizedEmail")] public NormalizedEmail NormalizedEmail { get; set; }
    
    [JsonPropertyName("HasAccessTo")] public HasAccessTo HasAccessTo { get; set; }
}

public record DataOld
{
    [JsonPropertyName("Email")] public Email Email { get; set; }

    [JsonPropertyName("NormalizedEmail")] public NormalizedEmail NormalizedEmail { get; set; }
}


public class HasAccessTo
{
    [JsonPropertyName("iv")]
    public List<string> ContentList { get; set; }
}

public record Payload
{
    [JsonPropertyName("$type")] public string EventType { get; set; }

    [JsonPropertyName("type")] public string Type { get; set; }

    [JsonPropertyName("id")] public string Id { get; set; }

    [JsonPropertyName("created")] public DateTime Created { get; set; }

    [JsonPropertyName("lastModified")] public DateTime LastModified { get; set; }

    [JsonPropertyName("createdBy")] public string CreatedBy { get; set; }

    [JsonPropertyName("lastModifiedBy")] public string LastModifiedBy { get; set; }

    [JsonPropertyName("data")] public Data Data { get; set; }

    [JsonPropertyName("dataOld")] public DataOld DataOld { get; set; }

    [JsonPropertyName("status")] public string Status { get; set; }

    [JsonPropertyName("partition")] public int Partition { get; set; }

    [JsonPropertyName("schemaId")] public string SchemaId { get; set; }

    [JsonPropertyName("actor")] public string Actor { get; set; }

    [JsonPropertyName("appId")] public string AppId { get; set; }

    [JsonPropertyName("timestamp")] public DateTime Timestamp { get; set; }

    [JsonPropertyName("name")] public string Name { get; set; }

    [JsonPropertyName("version")] public int Version { get; set; }
}

public record CustomerChangedEvent
{
    [JsonPropertyName("type")] public string Type { get; set; }

    [JsonPropertyName("payload")] public Payload Payload { get; set; }

    [JsonPropertyName("timestamp")] public DateTime Timestamp { get; set; }
}
