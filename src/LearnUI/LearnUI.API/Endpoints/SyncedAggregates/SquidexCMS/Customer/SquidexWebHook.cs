﻿using Carter;
using LearningApi.Features.SyncedAggregates.CMS.Content;
using LearnUI.API.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace LearnUI.API.Endpoints.SquidexSync.Customer;

public class SquidexWebHook : ICarterModule
{
    public void AddRoutes(IEndpointRouteBuilder app)
    {
        app.MapPost("/sync/squidex/customer", Handler)
            .ExcludeFromDescription();
    }

    private async Task<IResult> Handler(
        [FromServices] LearnUIDBContext dbContext, 
        CustomerChangedEvent eventData, 
        IOptions<SquidexOptions> squidexOptions, 
        HttpContext context)
    {
        if(!await context.SquidexSignatureValid(squidexOptions.Value.SigningSecret))
            return Results.Problem("Unable to validate signature");

        var customerRecord = await dbContext.Customer.FindAsync(new Guid(eventData.Payload.Id));

        if (customerRecord == null)
        {
            customerRecord = new Domain.SyncedAggregates.Customer();
            dbContext.Customer.Add(customerRecord);
        }

        customerRecord.Email = eventData.Payload.Data.Email.Iv;
        customerRecord.Id = new Guid(eventData.Payload.Id);
        customerRecord.LastUpdated = eventData.Payload.LastModified.ToUniversalTime();
        customerRecord.HasAccessTo = eventData.Payload.Data.HasAccessTo.ContentList.Select(x=>new Guid(x)).ToList();

        await dbContext.SaveChangesAsync();
        return Results.Ok();
    }
}