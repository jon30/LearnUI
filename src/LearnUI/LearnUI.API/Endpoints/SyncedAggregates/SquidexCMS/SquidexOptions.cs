﻿namespace LearnUI.API.Endpoints.SquidexSync.Customer;

public class SquidexOptions
{
    public string SigningSecret { get; set; }
}