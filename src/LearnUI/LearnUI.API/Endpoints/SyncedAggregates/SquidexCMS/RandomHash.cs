﻿using System.Security.Cryptography;
using System.Text;

namespace LearnUI.API.Endpoints.SquidexSync.Customer;

public static class RandomHash
{
    public static string Sha256Base64(this string value)
    {
        using var sha = SHA256.Create();
        var bytesValue = Encoding.UTF8.GetBytes(value);
        var bytesHash = sha.ComputeHash(bytesValue);
        var result = Convert.ToBase64String(bytesHash);
        return result;
    }
}