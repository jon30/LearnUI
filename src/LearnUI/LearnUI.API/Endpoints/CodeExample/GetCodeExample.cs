﻿using LearnUI.Contracts.CodeExample;
using MediatR;
using PracticalAspNet.Shared.Features.Content.Example;

namespace LearnUI.API.Endpoints.CodeExample;

public class GetCodeExampleHandler : IRequestHandler<GetCodeExampleRequest, GetCodeExampleResponse>
{
    private readonly CourseContentProviders _courseContentProviders;

    public GetCodeExampleHandler(CourseContentProviders courseContentProviders)
    {
        _courseContentProviders = courseContentProviders;
    }
    
    public async Task<GetCodeExampleResponse> Handle(
        GetCodeExampleRequest request,
        CancellationToken cancellationToken)
    {
        var result = new ExampleFolderDTO
        {
            RootNode = new ExampleFolderDTO.NodeDTO { Name = request.Identifier }
        };

        var tree = await _courseContentProviders
            .ForCourse(request.Course)
            .Demos
            .GetTreeRecursive(FileFilter.Default, request.Identifier, true);
        
        MapNodeDto(tree, result.RootNode);

        return new GetCodeExampleResponse { ExampleDto = result };
    }

    private static void MapNodeDto(TreeNode treeNode, ExampleFolderDTO.NodeDTO targetDTO)
    {
        foreach (var child in treeNode.Nodes)
        {
            var newNodeDto = new ExampleFolderDTO.NodeDTO
            {
                Name = child.Value.Path,
                Slug = child.Value.Slug,
                Content = child.Value.Contents
            };

            MapNodeDto(child.Value, newNodeDto);
            targetDTO.Nodes.Add(newNodeDto);
        }
    }
}

public class FileFilter
{
    public static bool Default(string filePath) => true;
}