﻿using Carter;
using LearnUI.Contracts.CodeExample;
using MediatR;

namespace LearnUI.API.CodeExamples;

public class CodeExampleModule : ICarterModule
{
    public void AddRoutes(IEndpointRouteBuilder app)
    {
        app.MapGet("codeExample/{course}/{identifier}",
            async (HttpContext HttpContext, string course, string identifier, IMediator _mediator)
                => await _mediator.Send(new GetCodeExampleRequest { Course = course, Identifier = identifier }));
    }
}