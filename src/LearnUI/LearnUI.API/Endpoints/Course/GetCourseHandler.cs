﻿using LearnUI.API.Domain;
using LearnUI.API.Endpoints.CodeExample;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LearnUI.API.Content;

public class GetCourseHandler : IRequestHandler<GetCourseRequest, GetCourseResponse>
{
    private readonly CourseContentProviders _contentsProviders;
    private readonly LearnUIDBContext _dbContext;

    public GetCourseHandler(CourseContentProviders contentsProviders, LearnUIDBContext dbContext)
    {
        _dbContext = dbContext;
        _contentsProviders = contentsProviders;
    }

    public async Task<GetCourseResponse> Handle(
        GetCourseRequest request,
        CancellationToken cancellationToken)
    {
        var courseContent = _contentsProviders.ForCourse(request.Course);
        var fileTree = await courseContent.Curriculum.GetTreeRecursive(x =>
            Path.GetExtension(x) == ".md" && !Path.GetFileName(x).StartsWith("_index"));

        var targetDto = new GetCourseResponse.CourseNodeDTO();
        MapNodeDto(fileTree, targetDto);

        var contentDetails = await _dbContext.Content.FirstOrDefaultAsync(x => x.ContentId == request.Course, cancellationToken: cancellationToken);
        
        return new GetCourseResponse
        {
            Curriculum = targetDto,
            Name = contentDetails?.Name,
            DirectPurchaseProductId = contentDetails?.DirectPurchaseProduct
        };
    }

    private static void MapNodeDto(TreeNode treeNode, GetCourseResponse.CourseNodeDTO targetDTO)
    {
        foreach (var child in treeNode.Nodes)
        {
            var newNodeDto = new GetCourseResponse.CourseNodeDTO
            {
                Name = child.Value.Path.ToLower(),
                Slug = child.Value.Slug.Replace("\\", "/").ToLower(),
                Type = child.Value.Nodes.Any()
                    ? NodeType.Module
                    : NodeType.Page
            };

            MapNodeDto(child.Value, newNodeDto);
            targetDTO.Nodes.Add(newNodeDto);
        }
    }
}