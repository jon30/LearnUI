﻿using Carter;
using MediatR;

namespace LearnUI.API.Content;

public class ContentModule : ICarterModule
{
    public void AddRoutes(IEndpointRouteBuilder app)
    {
        app.MapGet("course/{course}/", async (HttpContext context,string course, IMediator _mediator) 
            => await _mediator.Send(new GetCourseRequest{Course = course}));

        app.MapGet("course/{course}/{slug}", async (HttpContext context, string course, string slug, IMediator _mediator) 
            => await _mediator.Send(new GetPageRequest{Course = course, Slug = slug}));
    }
}