﻿using ColorCode.Styling;
using LearnUI.API.FileProviders;
using Markdig;
using Markdig.Extensions.Yaml;
using Markdig.Renderers;
using Markdig.Syntax;
using Markdig.Syntax.Inlines;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace LearnUI.API.Content;

/// <summary>
/// Reads course contents from file node
/// </summary>
public class Lesson
{
    public string Name { get; set; }
    public string Html { get; set; }
    public bool FreePreview { get; set; }

    private Lesson() { }
    
    public static async Task<Lesson> ForPath(IContentProvider courseContentProvider, string slug)
    {
        var file = await courseContentProvider.File(slug);
        
        var pipeline = new MarkdownPipelineBuilder()
            .UseCustomContainers()
            .UseGenericAttributes()
            .UseYamlFrontMatter()
            .UseSyntaxHighlighting(StyleDictionary.DefaultLight)
            .Build();

        var writer = new StringWriter();
        var renderer = new HtmlRenderer(writer);
        pipeline.Setup(renderer);

        var markdown = await file.ContentsAsync();
        var document = Markdown.Parse(markdown, pipeline);
        
        await ProcessImages(courseContentProvider, slug, document);
        var lessonFrontMatter = await ParseYAML(markdown,document, file);

        renderer.Render(document);
        await writer.FlushAsync();
        var html = writer.ToString();

        return new Lesson { 
            Html = html, 
            Name = lessonFrontMatter.Title,
            FreePreview = lessonFrontMatter.FreePreview
        };
    }

    private static async Task ProcessImages(IContentProvider courseContentProvider, string slug, MarkdownDocument document)
    {
        foreach (var inlineLink in document.Descendants<LinkInline>())
        {
            if (!inlineLink.IsImage) continue;

            var imageFile = await courseContentProvider.File(
                Path.Join(Path.GetDirectoryName(slug), inlineLink.Url));

            inlineLink.Url = await GetBase64ImageAsync(imageFile);
        }
    }

    private static async Task<LessonFrontMatter> ParseYAML(string markdown, MarkdownDocument document, IFileNode file)
    {
        var yamlBlock = document.Descendants<YamlFrontMatterBlock>().FirstOrDefault();

        if (yamlBlock != null)
        {
            var yaml = markdown.Substring(yamlBlock.Span.Start, yamlBlock.Span.Length).Replace("---", "");
            var deserializer = new DeserializerBuilder().WithNamingConvention(CamelCaseNamingConvention.Instance).Build();
            return deserializer.Deserialize<LessonFrontMatter>(yaml);
        }

        return new LessonFrontMatter { Title = "Unknown page", FreePreview = false};
    }

    public static async Task<string> GetBase64ImageAsync(IFileNode file)
    {
        var imageBytes = await file.BytesAsync();
        var base64String = Convert.ToBase64String(imageBytes);
        return $"data:{file.FileType};base64,{base64String}";
    }
}

public class LessonFrontMatter
{
    public string Title { get; set; }
    public bool FreePreview { get; set; }
}