﻿using System.Web;
using Dapper;
using LearnUI.API.Endpoints.CodeExample;
using LearnUI.API.FileProviders;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LearnUI.API.Content;

public class GetPageHandler : IRequestHandler<GetPageRequest, GetPageResponse>
{
    private readonly CourseContentProviders _courseContentProviders;
    private readonly DapperContext _dapperContext;

    public GetPageHandler(CourseContentProviders courseContentProviders, DapperContext dapperContext)
    {
        _dapperContext = dapperContext;
        _courseContentProviders = courseContentProviders;
    }

    public async Task<GetPageResponse> Handle(GetPageRequest request, CancellationToken cancellationToken)
    {
        // tidy up the incoming slug, and make sure we're looking for md files
        var slug = Path.ChangeExtension(HttpUtility.UrlDecode(request.Slug), ".md");

        var contentProviders = _courseContentProviders.ForCourse(request.Course);
        var courseContent = await Lesson.ForPath(contentProviders.Curriculum, slug);

        
        if (!courseContent.FreePreview)
        {
            using var conn = _dapperContext.CreateConnection();
            var sql = "SELECT content.\"Id\", content.\"Name\" " +
                      "FROM learnui.Customer customer " +
                      "JOIN learnui.Content content ON content.\"Id\"= ANY(customer.\"HasAccessTo\")";
        
            var result = await conn.QueryAsync(sql, new { request.Email });

            if (!result.Any())
                return new GetPageResponse { Locked = true };
        }

       
        return new GetPageResponse
        {
            Content = courseContent.Html,
            Name = courseContent.Name,
            Locked = ! courseContent.FreePreview
        };
    }
}