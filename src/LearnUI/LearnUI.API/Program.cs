using System.Reflection;
using Carter;
using LearnUI.API.Configuration;
using LearnUI.API.Content;
using LearnUI.API.Domain;
using LearnUI.API.Endpoints.CodeExample;
using LearnUI.API.Endpoints.SquidexSync.Customer;
using LearnUI.API.Infrastructure;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Logging;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddAuthorization();
builder.Services.AddCors();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.Configure<GithubOptions>(builder.Configuration.GetSection("Github"));
builder.Services.Configure<CourseContentOptions>(builder.Configuration.GetSection("CourseContent"));
builder.Services.Configure<SquidexOptions>(builder.Configuration.GetSection("Squidex"));

builder.Services.AddDbContext<LearnUIDBContext>(optionsBuilder =>
    optionsBuilder.UseNpgsql(builder.Configuration.GetConnectionString("Default"),
        x => x.MigrationsHistoryTable("__EFMigrations", "learnui")));

builder.Services.AddHostedService<MigratorHostedService>();

builder.Services.AddSingleton<DapperContext>();

builder.Services.AddTransient(typeof(IPipelineBehavior<,>), typeof(AuthenticatedRequestBehaviour<,>));
builder.Services.AddMediatR(Assembly.GetExecutingAssembly());

builder.Services.AddScoped<FileProviderFactory>();
builder.Services.AddSingleton<CourseContentProviders>();

builder.Services.AddHttpContextAccessor();
builder.Services.AddCarter();

builder.AddAuth0();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    IdentityModelEventSource.ShowPII = true;
}

app.UseWhen(
    ctx => ctx.Request.Path.StartsWithSegments("/sync/squidex"),
    ab => ab.UseMiddleware<EnableRequestBodyBufferingMiddleware>()
);

app.UseCors(options => options.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());
app.UseAuthentication();
app.UseAuthorization();

app.UseHttpsRedirection();
app.MapCarter();

app.Run();

// to ensure we can access this from tests
public partial class Program
{
}