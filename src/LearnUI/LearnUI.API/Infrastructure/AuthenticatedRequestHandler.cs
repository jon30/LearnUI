using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using MediatR;

namespace LearnUI.API.Infrastructure
{
    public class AuthenticatedRequestBehaviour<TRequest, TResponse>: IPipelineBehavior<TRequest, TResponse> where TRequest: IRequest<TResponse>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AuthenticatedRequestBehaviour(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<TResponse> Handle(TRequest request, RequestHandlerDelegate<TResponse> next, CancellationToken cancellationToken)
        {
            if (_httpContextAccessor?.HttpContext?.User == null)
                return await next();

            var user = _httpContextAccessor.HttpContext.User;
            var identifier = user.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            
            switch (request)
            {
                case AuthenticatedRequest<TResponse> authenticatedRequest:
                    authenticatedRequest.UserIdentifier = identifier;
                    authenticatedRequest.Email = Email(user);
                    break;
                case AuthenticatedRequest authenticatedRequest:
                    authenticatedRequest.UserIdentifier = identifier;
                    authenticatedRequest.Email = Email(user);
                    break;
            }
            
            return await next();
        }

        private string Email(ClaimsPrincipal user)
        {
            // try accessing the email claim, best to use if available...
            var email = user.FindFirst("https://practicaldotnet.io/claims/email");
            if (email != null)
                return email.Value;
           
            var idToken = _httpContextAccessor.HttpContext.Request.Headers["id_token"];

            JwtSecurityToken tokenInfo = null;
            if (!string.IsNullOrEmpty(idToken) && idToken != "undefined")
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                tokenInfo = tokenHandler.ReadJwtToken(idToken);
            }

            return tokenInfo?.Claims.SingleOrDefault(x => x.Type == "email")?.Value;

        }
    }
    
}