﻿using System.Data;
using Npgsql;

public class DapperContext
{
    private readonly IConfiguration _configuration;
    private readonly string _connectionString;
    public DapperContext(IConfiguration configuration)
    {
        _configuration = configuration;
        _connectionString = _configuration.GetConnectionString("Default");

        Dapper.DefaultTypeMap.MatchNamesWithUnderscores = true;

    }
    public IDbConnection CreateConnection()
        => new NpgsqlConnection(_connectionString);
}