﻿using LearnUI.API.Domain;
using Microsoft.EntityFrameworkCore;

public class MigratorHostedService: IHostedService
{
    private readonly IServiceProvider _serviceProvider;

    public MigratorHostedService(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }


    public async Task StartAsync(CancellationToken cancellationToken)
    {
        // Create a new scope to retrieve scoped services
        using var scope = _serviceProvider.CreateScope();
        // Get the DbContext instance
        var myDbContext = scope.ServiceProvider.GetRequiredService<LearnUIDBContext>();

        //Do the migration asynchronously
        await myDbContext.Database.MigrateAsync(cancellationToken: cancellationToken);
    }

    public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;
}