﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace LearnUI.API.Migrations
{
    /// <inheritdoc />
    public partial class TableNameCasing : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Customer",
                schema: "learnui",
                table: "Customer");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Content",
                schema: "learnui",
                table: "Content");

            migrationBuilder.RenameTable(
                name: "Customer",
                schema: "learnui",
                newName: "customer",
                newSchema: "learnui");

            migrationBuilder.RenameTable(
                name: "Content",
                schema: "learnui",
                newName: "content",
                newSchema: "learnui");

            migrationBuilder.AddPrimaryKey(
                name: "PK_customer",
                schema: "learnui",
                table: "customer",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_content",
                schema: "learnui",
                table: "content",
                column: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_customer",
                schema: "learnui",
                table: "customer");

            migrationBuilder.DropPrimaryKey(
                name: "PK_content",
                schema: "learnui",
                table: "content");

            migrationBuilder.RenameTable(
                name: "customer",
                schema: "learnui",
                newName: "Customer",
                newSchema: "learnui");

            migrationBuilder.RenameTable(
                name: "content",
                schema: "learnui",
                newName: "Content",
                newSchema: "learnui");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Customer",
                schema: "learnui",
                table: "Customer",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Content",
                schema: "learnui",
                table: "Content",
                column: "Id");
        }
    }
}
