﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace LearnUI.API.Migrations
{
    /// <inheritdoc />
    public partial class Updatingcoursedetails : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DirectPurchaseProduct",
                schema: "learnui",
                table: "content",
                type: "text",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DirectPurchaseProduct",
                schema: "learnui",
                table: "content");
        }
    }
}
