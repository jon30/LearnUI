﻿using LearnUI.API.FileProviders;

namespace LearnUI.API.Endpoints.CodeExample;

public record Providers(IContentProvider Curriculum, IContentProvider Demos);