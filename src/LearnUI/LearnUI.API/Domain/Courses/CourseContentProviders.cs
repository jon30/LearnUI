﻿using LearnUI.API.Configuration;
using LearnUI.API.FileProviders;
using Microsoft.Extensions.Options;

namespace LearnUI.API.Endpoints.CodeExample;

public class CourseContentProviders
{
    private readonly string _basePath;

    public CourseContentProviders(IOptions<CourseContentOptions> courseContentOptions)
    {
        _basePath = courseContentOptions.Value.Path;
    }

    public Providers ForCourse(string courseId)
    {
        return new Providers(
            new FileContentProvider(Path.Combine(_basePath, courseId, "content")),
            new FileContentProvider(Path.Combine(_basePath, courseId, "examples"))
        );
    }
}