﻿namespace LearnUI.API.FileProviders;

public interface IContentProvider
{
    public Task<TreeNode> GetTreeRecursive(Func<string, bool>? filter, string rootPath = "", bool includeContents = false);

    public Task<IFileNode> File(string path);
}