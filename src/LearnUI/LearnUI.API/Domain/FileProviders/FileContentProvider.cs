namespace LearnUI.API.FileProviders;

public class FileContentProvider : IContentProvider
{
    private readonly string _basePath;

    public FileContentProvider(string basePath)
    {
        _basePath = basePath;
    }

    public async Task<TreeNode> GetTreeRecursive(Func<string, bool>? filter, string rootPath = "",
        bool includeContents = false)
    {
        var path = Path.GetFullPath(Path.Combine(_basePath, rootPath));

        var pathList = new List<string>();
        PopulatePaths(path, pathList, filter);

        var node = new TreeNode(path, '\\');

        foreach (var subPath in pathList)
        {
            var fileContents = "";
            if (includeContents && !string.IsNullOrEmpty(Path.GetExtension(subPath)))
                fileContents = await System.IO.File.ReadAllTextAsync(subPath);
            node.AddPath(subPath, fileContents);
        }

        return node;
    }

    public async Task<IFileNode> File(string path)
    {
        var contentPath = Path.GetFullPath(Path.Join(_basePath, path));
        var node = FileSystemFileNode.ForPath(contentPath);
        return node;
    }

    private static void PopulatePaths(
        string contentPath,
        List<string> paths,
        Func<string, bool> filter)
    {
        var directories = Directory.GetDirectories(contentPath);
        paths.AddRange(directories);
        paths.AddRange(Directory
            .GetFiles(contentPath)
            .Where(s => filter != null && filter(s))
            .Select(x => x.Replace(".md", ""))
        );

        foreach (var subfolder in directories)
        {
            PopulatePaths(subfolder, paths, filter);
        }
    }
}