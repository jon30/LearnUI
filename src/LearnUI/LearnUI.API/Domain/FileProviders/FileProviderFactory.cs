using LearnUI.API.FileProviders;

namespace LearnUI.API.Endpoints.CodeExample;

public class FileProviderFactory
{
    public FileContentProvider FileSystem(string basePath) => new(basePath);
}