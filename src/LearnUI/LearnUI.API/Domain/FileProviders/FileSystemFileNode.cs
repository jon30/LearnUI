﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.StaticFiles;

namespace LearnUI.API.FileProviders;

public interface IFileNode
{
    public string ContentPath { get;}
    public string FileType { get; }
    public Task<string> ContentsAsync();
    public Task<byte[]> BytesAsync();
    public string Name();
}

public class FileSystemFileNode : IFileNode
{
    private string _path;
    public string ContentPath => _path;
    public async Task<string> ContentsAsync() => await File.ReadAllTextAsync(ContentPath);

    public async Task<byte[]> BytesAsync() => await File.ReadAllBytesAsync(ContentPath);
    string IFileNode.Name() => Path.GetFileName(_path);

    public string FileType => new FileExtensionContentTypeProvider().TryGetContentType(_path, out string contentType)
        ? contentType
        : throw new UnsupportedContentTypeException("Could not determine content type");

    public string Name => Path.GetFileName(_path);

    public static FileSystemFileNode ForPath(string path)
    {
        return new FileSystemFileNode
        {
            _path = path
        };
    }
}