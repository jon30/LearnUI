﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.StaticFiles;

public class TreeNode
{
    private readonly IDictionary<string, TreeNode> _nodes = new Dictionary<string, TreeNode>();
    private readonly string _basePath;
    private readonly char _separator;
    public IDictionary<string, TreeNode> Nodes => _nodes;

    public string Path { get; private init; }

    public string Slug { get; private init; }

    public string Contents { get; private init; }

    public TreeNode(string basePath, char separator = '/')
    {
        _separator = separator;
        _basePath = basePath;
    }

    public void AddPath(string path, string contents = "")
    {
        var charSeparators = new[] { _separator };

        var parts = path.Replace(_basePath, "").Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);

        var currentNode = this;

        foreach (var part in parts)
        {
            TreeNode child;

            if (!currentNode._nodes.TryGetValue(part, out child))
            {
                child = new TreeNode(_basePath)
                {
                    Path = part,
                    Slug = path.Replace(_basePath, ""),
                    Contents = contents
                };

                // Add to the dictionary.
                currentNode._nodes[part] = child;
            }

            currentNode = child;
        }
    }
}