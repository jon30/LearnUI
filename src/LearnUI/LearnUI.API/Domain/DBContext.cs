﻿using LearnUI.API.Domain.SyncedAggregates;
using Microsoft.EntityFrameworkCore;

namespace LearnUI.API.Domain;

public class LearnUIDBContext : DbContext
{
    public DbSet<Customer> Customer { get; set; }
    public DbSet<Course> Content { get; set; }
    
    public LearnUIDBContext(DbContextOptions options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.HasDefaultSchema("learnui");

        modelBuilder.Entity<Course>().Property(e => e.LastUpdated)
            .HasDefaultValueSql("CURRENT_TIMESTAMP")
            .ValueGeneratedOnAdd();
        
        modelBuilder.Entity<Customer>().Property(e => e.LastUpdated)
            .HasDefaultValueSql("CURRENT_TIMESTAMP")
            .ValueGeneratedOnAdd();
        
        base.OnModelCreating(modelBuilder);
    }
}