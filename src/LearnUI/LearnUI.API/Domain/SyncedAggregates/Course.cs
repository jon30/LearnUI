﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LearnUI.API.Domain.SyncedAggregates;

[Table("content")]
public class Course : IEntity
{
    public Guid Id { get; set; }
    public string ContentId { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public string Format { get; set; }
    public DateTime LastUpdated { get; set; }
    public string DirectPurchaseProduct { get; set; }
}