﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LearnUI.API.Domain.SyncedAggregates;

[Table("customer")]
public class Customer : IEntity
{
    public Guid Id { get; set; }
    public string Email { get; set; }
    public List<Guid>? HasAccessTo { get; set; }
    public DateTime LastUpdated { get; set; }
}