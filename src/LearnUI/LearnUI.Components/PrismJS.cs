﻿using Microsoft.JSInterop;

namespace LearnUI.Components;

public interface IPrismJS : IAsyncDisposable
{
    ValueTask<string> Highlight(string code, string language);
    ValueTask HighlightAll();
}

public class PrismJS : IPrismJS
{
    private readonly Lazy<Task<IJSObjectReference>> moduleTask;

    public PrismJS(IJSRuntime jsRuntime)
    {
        moduleTask = new(() => jsRuntime.InvokeAsync<IJSObjectReference>(
            "import", "./_content/LearnUI.Components/sourcecode.js").AsTask());
    }

    public async ValueTask<string> Highlight(string code, string language)
    {
        var module = await moduleTask.Value;
        return await module.InvokeAsync<string>("highlight", code, language);
    }

    public async ValueTask HighlightAll()
    {
        var module = await moduleTask.Value;
        await module.InvokeVoidAsync("highlightAll");
    }

    public async ValueTask DisposeAsync()
    {
        if (moduleTask.IsValueCreated)
        {
            var module = await moduleTask.Value;
            await module.DisposeAsync();
        }
    }
}