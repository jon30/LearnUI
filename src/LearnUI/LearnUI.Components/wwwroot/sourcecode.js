export function highlight(code, lang){
    if (window.Prism) {
        try {

            if (Prism.languages[lang]) {
                return window.Prism.highlight(code, Prism.languages[lang], lang);
            } else {
                console.log('no lang found!')
                return Prism.util.encode(code, lang)
            }


        } catch (e) {
            console.error(e);
        }
    }    
}

export function highlightAll(){
    if(window.Prism){
        window.Prism.highlightAll();
    }
}
