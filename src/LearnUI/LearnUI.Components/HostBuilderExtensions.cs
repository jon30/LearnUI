﻿using LearnUI.Components.Features.Content.Widgets.FileViewer;
using LearnUI.Contracts;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Refit;
using UI.Client.Infrastructure;

namespace LearnUI.Components;

public static class WASMConfigExtensions
{
    public static WebAssemblyHostBuilder UseLearnUI(this WebAssemblyHostBuilder builder, string apiURL)
    {
        builder.RootComponents.RegisterCustomElement<FileViewer>("file-viewer");
        builder.Services.AddScoped<IPrismJS, PrismJS>();

        builder.Services.AddScoped<NoExceptionAuthorizationMessageHandler>();
        
        builder.Services.AddRefitClient<Api>()
            .ConfigureHttpClient(c => { c.BaseAddress = new Uri(apiURL); })
            .AddHttpMessageHandler(sp => sp.GetRequiredService<NoExceptionAuthorizationMessageHandler>()
                .ConfigureHandler(new[] { apiURL }));
        
        return builder;
    }
}