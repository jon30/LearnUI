﻿using LearnUI.API.Domain;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Npgsql;
using Respawn;
using Respawn.Graph;


[CollectionDefinition(nameof(SliceFixture))]
public class SliceFixtureCollection : ICollectionFixture<SliceFixture>
{
}

public class SliceFixture : IAsyncLifetime
{
    private Respawner _respawner;
    private IConfiguration _configuration => _factory.Services.GetRequiredService<IConfiguration>();
    private IServiceScopeFactory _scopeFactory => _factory.Services.GetRequiredService<IServiceScopeFactory>();
    private readonly WebApplicationFactory<Program> _factory;

    public SliceFixture()
    {
        _factory = new LearnUITestApplicationFactory();
    }

    class LearnUITestApplicationFactory
        : WebApplicationFactory<Program>
    {
        protected override IHost CreateHost(IHostBuilder builder)
        {
            builder.ConfigureHostConfiguration(configBuilder =>
            {
                var config = new Dictionary<string, string>
                {
                    { "ConnectionStrings:Default", _connectionString },
                    { "CourseContent:Path", "TestContent" }
                };

                configBuilder.AddInMemoryCollection(config);
            });

            return base.CreateHost(builder);
        }

        private readonly string _connectionString =
            "Host=127.0.0.1;Database=learnuitests;Username=admin;Password=secret;Port=5432";
    }

    public async Task ExecuteScopeAsync(Func<IServiceProvider, Task> action)
    {
        using var scope = _scopeFactory.CreateScope();
        var dbContext = scope.ServiceProvider.GetRequiredService<LearnUIDBContext>();

        try
        {
            // await dbContext.BeginTransactionAsync();

            await action(scope.ServiceProvider);

            //await dbContext.CommitTransactionAsync();
        }
        catch (Exception)
        {
            // dbContext.RollbackTransaction(); 
            throw;
        }
    }

    public async Task<T> ExecuteScopeAsync<T>(Func<IServiceProvider, Task<T>> action)
    {
        using var scope = _scopeFactory.CreateScope();
        var dbContext = scope.ServiceProvider.GetRequiredService<LearnUIDBContext>();

        try
        {
            //await dbContext.BeginTransactionAsync();

            var result = await action(scope.ServiceProvider);

            //await dbContext.CommitTransactionAsync();

            return result;
        }
        catch (Exception)
        {
            //dbContext.RollbackTransaction();
            throw;
        }
    }

    public Task ExecuteDbContextAsync(Func<LearnUIDBContext, Task> action)
        => ExecuteScopeAsync(sp => action(sp.GetService<LearnUIDBContext>()));

    public Task ExecuteDbContextAsync(Func<LearnUIDBContext, ValueTask> action)
        => ExecuteScopeAsync(sp => action(sp.GetService<LearnUIDBContext>()).AsTask());

    public Task ExecuteDbContextAsync(Func<LearnUIDBContext, IMediator, Task> action)
        => ExecuteScopeAsync(sp => action(sp.GetService<LearnUIDBContext>(), sp.GetService<IMediator>()));

    public Task<T> ExecuteDbContextAsync<T>(Func<LearnUIDBContext, Task<T>> action)
        => ExecuteScopeAsync(sp => action(sp.GetService<LearnUIDBContext>()));

    public Task<T> ExecuteDbContextAsync<T>(Func<LearnUIDBContext, ValueTask<T>> action)
        => ExecuteScopeAsync(sp => action(sp.GetService<LearnUIDBContext>()).AsTask());

    public Task<T> ExecuteDbContextAsync<T>(Func<LearnUIDBContext, IMediator, Task<T>> action)
        => ExecuteScopeAsync(sp => action(sp.GetService<LearnUIDBContext>(), sp.GetService<IMediator>()));

    public Task InsertAsync<T>(params T[] entities) where T : class
    {
        return ExecuteDbContextAsync(db =>
        {
            foreach (var entity in entities)
            {
                db.Set<T>().Add(entity);
            }

            return db.SaveChangesAsync();
        });
    }

    public Task InsertAsync<TEntity>(TEntity entity) where TEntity : class
    {
        return ExecuteDbContextAsync(db =>
        {
            db.Set<TEntity>().Add(entity);

            return db.SaveChangesAsync();
        });
    }

    public Task InsertAsync<TEntity, TEntity2>(TEntity entity, TEntity2 entity2)
        where TEntity : class
        where TEntity2 : class
    {
        return ExecuteDbContextAsync(db =>
        {
            db.Set<TEntity>().Add(entity);
            db.Set<TEntity2>().Add(entity2);

            return db.SaveChangesAsync();
        });
    }

    public Task InsertAsync<TEntity, TEntity2, TEntity3>(TEntity entity, TEntity2 entity2, TEntity3 entity3)
        where TEntity : class
        where TEntity2 : class
        where TEntity3 : class
    {
        return ExecuteDbContextAsync(db =>
        {
            db.Set<TEntity>().Add(entity);
            db.Set<TEntity2>().Add(entity2);
            db.Set<TEntity3>().Add(entity3);

            return db.SaveChangesAsync();
        });
    }

    public Task InsertAsync<TEntity, TEntity2, TEntity3, TEntity4>(TEntity entity, TEntity2 entity2, TEntity3 entity3,
        TEntity4 entity4)
        where TEntity : class
        where TEntity2 : class
        where TEntity3 : class
        where TEntity4 : class
    {
        return ExecuteDbContextAsync(db =>
        {
            db.Set<TEntity>().Add(entity);
            db.Set<TEntity2>().Add(entity2);
            db.Set<TEntity3>().Add(entity3);
            db.Set<TEntity4>().Add(entity4);

            return db.SaveChangesAsync();
        });
    }

    public Task<T> FindAsync<T>(int id)
        where T : class, IEntity
    {
        return ExecuteDbContextAsync(db => db.Set<T>().FindAsync(id).AsTask());
    }

    public Task<TResponse> SendAsync<TResponse>(IRequest<TResponse> request)
    {
        return ExecuteScopeAsync(sp =>
        {
            var mediator = sp.GetRequiredService<IMediator>();

            return mediator.Send(request);
        });
    }

    public Task SendAsync(IRequest request)
    {
        return ExecuteScopeAsync(sp =>
        {
            var mediator = sp.GetRequiredService<IMediator>();

            return mediator.Send(request);
        });
    }


    public async Task InitializeAsync()
    {
        var connectionString = _configuration.GetConnectionString("Default");
        await using var conn = new NpgsqlConnection(connectionString);
        await conn.OpenAsync();
        
        _respawner = await Respawner.CreateAsync(conn, new RespawnerOptions
        {
            SchemasToInclude = new[] { "learnui" },
            TablesToIgnore = new[]{new Table("__EFMigrations")},
            DbAdapter = DbAdapter.Postgres
        });

        await _respawner.ResetAsync(conn);
    }

    public Task DisposeAsync()
    {
        _factory?.Dispose();
        return Task.CompletedTask;
    }
}