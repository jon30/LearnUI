﻿{
  Curriculum: {
    Nodes: [
      {
        Name: module1,
        Slug: /module1,
        Nodes: [
          {
            Name: page1,
            Slug: /module1/page1,
            Type: Page
          }
        ],
        Type: Module
      }
    ]
  }
}