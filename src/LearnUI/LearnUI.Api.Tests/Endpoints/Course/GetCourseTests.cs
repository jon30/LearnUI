﻿using LearnUI.API.Content;

namespace LearnUI.Api.Tests;

[Collection(nameof(SliceFixture))]
[UsesVerify]
public class GetCourseTests
{
    private readonly SliceFixture _fixture;
    public GetCourseTests(SliceFixture fixture) => _fixture = fixture;
    
    [Fact]
    public async Task ListContents()
    {
        var request = new GetCourseRequest() { Course = "demo" };
        var result = await _fixture.SendAsync(request);
        await Verify(result);
    }

}