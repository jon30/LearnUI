﻿using LearnUI.API.Content;
using LearnUI.API.Domain.SyncedAggregates;
using Shouldly;

namespace LearnUI.Api.Tests;

[Collection(nameof(SliceFixture))]
public class GetPageWhenDirectPurchaseEnabled 
{
    private readonly SliceFixture _fixture;
    public GetPageWhenDirectPurchaseEnabled(SliceFixture fixture) => _fixture = fixture;

    [Fact]
    public async Task GetCourseIncludesDirectPurchaseProduct()
    {
        await _fixture.InsertAsync(new Course
        {
            ContentId = "demo", 
            DirectPurchaseProduct = "buymeid",
            Id = Guid.NewGuid(),
            Name = "Test Course",
            Description = "Test Description",
            Format = "Course"
        });
        
        var request = new GetCourseRequest
            { Course = "demo" };
        var result = await _fixture.SendAsync(request);
        
        result.DirectPurchaseProductId.ShouldBe("buymeid");
        
    }

}