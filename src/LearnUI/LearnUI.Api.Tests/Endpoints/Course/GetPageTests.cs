using LearnUI.API.Configuration;
using LearnUI.API.Content;
using LearnUI.API.Domain.SyncedAggregates;
using LearnUI.API.Endpoints.CodeExample;
using Microsoft.Extensions.Options;

namespace LearnUI.Api.Tests;

[Collection(nameof(SliceFixture))]
[UsesVerify]
public class ContentTests
{
    private readonly SliceFixture _fixture;
    public ContentTests(SliceFixture fixture) => _fixture = fixture;

    [Fact]
    public async Task GetWithUrlEncodedSlug()
    {
        var request = new GetPageRequest
            { Course = "demo", Slug = "%2FModule1%2FPage1" };

        var result = await _fixture.SendAsync(request);
        await Verify(result);
    }

    [Fact]
    public async Task GetWithNonUrlEncodedSlugWithBackslashes()
    {
        var request = new GetPageRequest
            { Course = "demo", Slug = "\\Module1\\Page1" };

        var result = await _fixture.SendAsync(request);
        await Verify(result);
    }

    [Fact]
    public async Task GetWithNonUrlEncodedSlugWithForwardSlashes()
    {
        var request = new GetPageRequest
            { Course = "demo", Slug = "/Module1/Page1" };
        var result = await _fixture.SendAsync(request);
        await Verify(result);
    }

    [Fact]
    public async Task GetWithDifferentlyCasedCourseIdentifier()
    {
        var request = new GetPageRequest
            { Course = "Demo", Slug = "/Module1/Page1" };
        var result = await _fixture.SendAsync(request);
        await Verify(result);
    }

    [Fact]
    public async Task GetWithDifferentlyCasedModuleSlug()
    {
        var request = new GetPageRequest
            { Course = "Demo", Slug = "/module1/page1" };
        var result = await _fixture.SendAsync(request);
        await Verify(result);
    }

    [Fact]
    public async Task LessonImagesAreBase64Encoded()
    {
        var request = new GetPageRequest
            { Course = "ImagesDemo", Slug = "/module1/001-example.md" };
        var result = await _fixture.SendAsync(request);
        await Verify(result);
    }
}