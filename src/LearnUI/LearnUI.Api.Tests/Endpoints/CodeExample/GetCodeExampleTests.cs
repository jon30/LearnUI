﻿using LearnUI.API.Configuration;
using LearnUI.API.Endpoints.CodeExample;
using LearnUI.Contracts.CodeExample;
using Microsoft.Extensions.Options;

namespace LearnUI.Api.Tests;

[UsesVerify]
public class CodeDemoTests
{
    private readonly IOptions<CourseContentOptions> _courseContentOptions = Options.Create(
        new CourseContentOptions
        {
            Path = @"C:\Users\hilto\RiderProjects\LearnUI\courses"
        });

    [Fact]
    public async Task GetShouldReturnDemoCode()
    {
        var handler = new GetCodeExampleHandler(new CourseContentProviders(_courseContentOptions));
        var result = await handler.Handle(new GetCodeExampleRequest { Course = "demo", Identifier = "EXAMPLE01" },
            CancellationToken.None);
        await Verify(result);
    }
}