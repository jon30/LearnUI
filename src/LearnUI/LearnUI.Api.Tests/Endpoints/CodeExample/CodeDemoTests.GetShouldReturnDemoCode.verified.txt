﻿{
  ExampleDto: {
    RootNode: {
      Name: EXAMPLE01,
      Nodes: [
        {
          Name: test,
          Slug: \test,
          Content: ,
          Nodes: [
            {
              Name: test.txt,
              Slug: \test\test.txt,
              Content: This is a test file
            }
          ]
        },
        {
          Name: helloworld.cs,
          Slug: \helloworld.cs,
          Content:
public class HelloWorld
{
    
}
        },
        {
          Name: test.html,
          Slug: \test.html,
          Content: <p>Hello World</p>
        }
      ]
    }
  }
}