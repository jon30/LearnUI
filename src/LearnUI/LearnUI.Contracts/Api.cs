﻿using LearnUI.API.Content;
using LearnUI.Contracts.CodeExample;
using Refit;

namespace LearnUI.Contracts;

public interface Api
{
    [Get("/codeExample/{request.course}/{request.identifier}")]
    Task<GetCodeExampleResponse> GetCodeExample(GetCodeExampleRequest request);
    
    [Get("/course/{request.course}")]
    Task<GetCourseResponse> GetCourse(GetCourseRequest request);

    [Get("/course/{request.course}/{request.slug}")]
    Task<GetPageResponse> GetPage(GetPageRequest request);
}