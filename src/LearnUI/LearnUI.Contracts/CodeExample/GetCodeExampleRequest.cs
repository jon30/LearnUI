﻿using MediatR;
using PracticalAspNet.Shared.Features.Content.Example;

namespace LearnUI.Contracts.CodeExample;

public class GetCodeExampleRequest : IRequest<GetCodeExampleResponse>
{
    public string Identifier { get; set; }
    public string Course { get; set; }
}

public class GetCodeExampleResponse
{
    public ExampleFolderDTO ExampleDto { get; set; }
}