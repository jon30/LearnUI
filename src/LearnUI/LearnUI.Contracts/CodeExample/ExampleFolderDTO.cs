﻿using System.Collections.Generic;

namespace PracticalAspNet.Shared.Features.Content.Example;

public class ExampleFolderDTO
{
    public NodeDTO RootNode { get; set; }

    public class NodeDTO
    {
        public string Name { get; set; }
        public string Slug { get; set; }
        
        public string Content { get; set; }

        public List<NodeDTO> Nodes { get; set; } = new();
    }
}