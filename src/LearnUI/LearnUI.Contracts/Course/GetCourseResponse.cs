﻿using PracticalAspNet.Shared.Features.Content.Example;

namespace LearnUI.API.Content;

public class GetCourseResponse
{
    public string Name { get; set; }
    public CourseNodeDTO Curriculum { get; set; }
    public string DirectPurchaseProductId { get; set; }

    public class CourseNodeDTO
    {
        public string Name { get; set; }
        public string Slug { get; set; }
        public List<CourseNodeDTO> Nodes { get; set; } = new();
        public NodeType Type { get; set; }

        public int LessonCount()
        {
            int total = CountNodes(Nodes, 0);
            return total;
        }

        private int CountNodes(List<CourseNodeDTO> nodes,  int total = 0)
        {
            foreach (var node in nodes)
            {
                if(node.Type == NodeType.Page)
                   total += 1;
                total= CountNodes(node.Nodes, total);
            }

            return total;

        }
    }
}

public enum NodeType
{
    Module = 1,
    Page = 2
}