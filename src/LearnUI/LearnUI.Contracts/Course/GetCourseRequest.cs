﻿using LearnUI.API.Infrastructure;

namespace LearnUI.API.Content;

public class GetCourseRequest : AuthenticatedRequest<GetCourseResponse>
{
    public string Course { get; set; }
}