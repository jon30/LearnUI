﻿using LearnUI.API.Infrastructure;

namespace LearnUI.API.Content;

public class GetPageRequest : AuthenticatedRequest<GetPageResponse>
{
    public string Course { get; set; }
    public string Slug { get; set; }
}

public class GetPageResponse
{
    public string Content { get; set; }
    public string Name { get; set; }
    public bool Locked { get; set; }
    public string DirectPurchaseProduct { get; set; }
}