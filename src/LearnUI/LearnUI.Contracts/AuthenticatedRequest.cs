﻿using MediatR;

namespace LearnUI.API.Infrastructure;

public abstract class AuthenticatedRequest : IRequest
{
    public string UserIdentifier { get; set; }
    public string Email { get; set; }
}

public abstract class AuthenticatedRequest<TResponse> : IRequest<TResponse>
{
    public string UserIdentifier { get; set; }
    public string Email { get; set; }
}