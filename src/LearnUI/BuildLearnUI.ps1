﻿Set-Location ".\"

dotnet build LearnUI.sln
# Creates nuget packages (in .nupkg files) in ./nupkgs
dotnet pack -o nupkgs
# Publishes all nuget packages in ./nupkgs to the local nuget repository
dotnet nuget push "nupkgs\**\*.nupkg" --source local