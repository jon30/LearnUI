module.exports = {
    content: [
        './**/*.razor',
        './**/*.cshtml',
        '../../**/*.Components/**/*.razor',
        '**/*.css'
    ],
    darkMode: 'class', // or 'media' or 'class'
    theme: {
        extend: {
            colors: {
                primary: 'rgb(112, 91, 160)'
            }
        },
    },
    variants: {
        extend: {
            textColor: ['disabled'],
            backgroundColor: ['disabled'],
            opacity: ['disabled'],
            transform: ['disabled'],
            borderStyle: ['dark']
        }
    }
}
