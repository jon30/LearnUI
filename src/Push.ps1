﻿# Taken from MediatR.Extensions.DependencyInjection https://github.com/jbogard/MediatR.Extensions.Microsoft.DependencyInjection/

$scriptName = $MyInvocation.MyCommand.Name
$artifacts = "./artifacts"

if ([string]::IsNullOrEmpty($Env:NUGET_API_USER)) {
    Write-Host "${scriptName}: NUGET_API_USER is empty or not set. Skipped pushing package(s)."
} else {
    dotnet nuget add source https://nuget.pkg.jetbrains.space/practicaldotnet/p/practical-asp-net/nuget/v3/index.json -n space-nuget -u $Env:NUGET_API_USER -p $Env:NUGET_API_TOKEN

    Get-ChildItem $artifacts -Filter "*.nupkg" | ForEach-Object {
        Write-Host "$($scriptName): Pushing $($_.Name)"
           dotnet nuget push $_ --source $Env:NUGET_URL 
        if ($lastexitcode -ne 0) {
            throw ("Exec: " + $errorMessage)
        }
    }
}