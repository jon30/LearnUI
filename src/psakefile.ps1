﻿properties {
    $configuration = 'Release'
    $owner = 'Jon Hilton'
    $product = 'LearnUI'
    $yearInitiated = '2022'
    $projectRootDirectory = "$(resolve-path .)"
    $publish = "$projectRootDirectory/publish"
    $testResults = "$projectRootDirectory/TestResults"
}

task Info -description "Display runtime information" {
    exec { dotnet --info }
}

task MigrateTest -description "Migrate Test DB" {
    exec { dotnet ef database update --connection "Host=127.0.0.1;Database=learnuitests;Username=admin;Password=secret;Port=5432" } -workingDirectory ./LearnUI/LearnUI.API
}

task Compile -depends Info -description "Compile the solution" {
    exec { dotnet build --configuration $configuration --nologo -p:"Product=$($product)"} -workingDirectory .
}

task Test -depends Compile, MigrateTest -description "Run unit tests" {
    # find any directory that ends in "Tests" and execute a test
    exec { dotnet test --configuration $configuration --no-build } -workingDirectory .
}