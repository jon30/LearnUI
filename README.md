# LearnUI

There are a few more projects than I'd typically try to have for something like this but the main reason for that is my use of a Razor Class Library (RCL).

I want to be able to (re)use the core parts of the UI for this in more than one project, so to that end the most interesting components are contained within the `LearnUI.Components` RCL.

In there you'll find the `FileViewer` component, which is a handy way to present source code to site visitors.

You'll also find the "Courseware" itself (check out the `Course` folder in `LearnUI.Components`).

There's a separate ASP.NET Core Hosted Blazor WASM Demo site which makes use of `LearnUI.Components` to render courses.

To launch the demo site, you'll need to start BOTH of these projects:

- `LearnUI.Demo.Server`
- `LearnUI.API`

But before you do, be sure to head over to `LearnUI.API\appsettings.Development.json` and update the `CourseContent:Path` to the correct location on your machine.

``` json
 "CourseContent": {
    "Path": "<repoLocation>\\courses"
  }  
```

The Api uses that path to locate the demo courses/code examples.

Here's a picture of how all the projects are connected:

![LearnUI](https://user-images.githubusercontent.com/34043583/192581229-79071691-4104-46f7-8528-a544e0060900.png)

As you can see from the diagram, the LearnUI project is effectively self-contained, with its own backend and frontend tests, plus a shared contracts folder between the Blazor WASM client and ASP.NET Core Web API backend.
